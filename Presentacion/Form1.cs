﻿using Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            clsNegPerson np = new clsNegPerson();
            dt = np.GetAll();

            dgvDatos.DataSource = dt;
            dgvDatos.Refresh();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            string personID = txtPersonID.Text.Trim();
            string lastname = txtLastname.Text.Trim();
            string firstname = txtFirstname.Text.Trim();
            if (!String.IsNullOrEmpty(personID) || !String.IsNullOrEmpty(lastname)
                 || !String.IsNullOrEmpty(firstname))
            {
                DataTable dt = new DataTable();
                clsNegPerson np = new clsNegPerson();
                dt = np.BuscarPersona(personID, lastname, firstname);
                dgvDatos.DataSource = dt;
                dgvDatos.Refresh();
            }
        }
    }
}
