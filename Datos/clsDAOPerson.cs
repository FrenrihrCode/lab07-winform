﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class clsDAOPerson : clsDAO
    {
        public DataTable GetAll()
        {
            DataTable dt = new DataTable();

            conn.Open();
            string sql = "SELECT * from Person";
            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            conn.Close();

            return dt;
        }

        public DataTable BuscarPersona(string personID, string lastname, string firstname)
        {
            try
            {
                DataTable dt = new DataTable();
                Console.WriteLine(personID + lastname + firstname);
                conn.Open();
                string sp = "BuscarPersonaByManyCamps";
                SqlCommand cmd = new SqlCommand(sp, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PersonID", SqlDbType.Int);
                cmd.Parameters.Add("@LastName", SqlDbType.NVarChar);
                cmd.Parameters.Add("@FirstName", SqlDbType.NVarChar);

                if (int.TryParse(personID, out int numberPersonID))
                    cmd.Parameters["@PersonID"].Value = personID;
                else
                    cmd.Parameters["@PersonID"].Value = DBNull.Value;
                cmd.Parameters["@LastName"].Value = lastname;
                cmd.Parameters["@FirstName"].Value = firstname;

                SqlDataReader reader = cmd.ExecuteReader();
                dt.Load(reader);
                conn.Close();

                return dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }
    }
}
